#include "kernelvaluesdialog.h"
#include "ui_kernelvaluesdialog.h"

KernelValuesDialog::KernelValuesDialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::KernelValuesDialog)
{
    _ui->setupUi(this);
}

KernelValuesDialog::~KernelValuesDialog()
{
    delete _ui;
}

void KernelValuesDialog::on_buttonBox_accepted()
{
    _kernel[0][0] = _ui->line00->text().toDouble();
    _kernel[0][1] = _ui->line01->text().toDouble();
    _kernel[0][2] = _ui->line02->text().toDouble();
    _kernel[1][0] = _ui->line10->text().toDouble();
    _kernel[1][1] = _ui->line11->text().toDouble();
    _kernel[1][2] = _ui->line12->text().toDouble();
    _kernel[2][0] = _ui->line20->text().toDouble();
    _kernel[2][1] = _ui->line21->text().toDouble();
    _kernel[2][2] = _ui->line22->text().toDouble();
    this->close();
}
