#include "coloradjustdialog.h"
#include "ui_coloradjustdialog.h"
#include "fotolojaimage.h"

ColorAdjustDialog::ColorAdjustDialog(FotoLojaImage image, QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::ColorAdjustDialog)
{
    _ui->setupUi(this);

    _cur_image = image;
    _preview_image = _cur_image;

    _scene_adj_before = new QGraphicsScene(this);
    _scene_adj_after = new QGraphicsScene(this);

    _scene_adj_before->clear();
    _scene_adj_before->addPixmap(_cur_image.ToQPixmap());
    _scene_adj_before->setSceneRect(_preview_image.ToQPixmap().rect());
    _ui->view_adj_before->setScene(_scene_adj_before);
    _ui->view_adj_before->fitInView(_scene_adj_before->itemsBoundingRect(), Qt::KeepAspectRatio);

    _scene_adj_after->clear();
    _scene_adj_after->addPixmap(_preview_image.ToQPixmap());
    _scene_adj_after->setSceneRect(_preview_image.ToQPixmap().rect());
    _ui->view_adj_after->setScene(_scene_adj_after);
    _ui->view_adj_after->fitInView(_scene_adj_after->itemsBoundingRect(), Qt::KeepAspectRatio);
}

ColorAdjustDialog::~ColorAdjustDialog()
{
    delete _ui;
    delete _scene_adj_before;
    delete _scene_adj_after;
}

/** HELPERS **/

void ColorAdjustDialog::UpdatePreview()
{
    _preview_image = _cur_image;
    int brightness_factor = _ui->bright_slider->value();
    int contrast_factor = _ui->contr_slider->value();
    _preview_image.AdjustBrightness(brightness_factor);
    _preview_image.AdjustContrast(contrast_factor);

    _scene_adj_after->clear();
    _scene_adj_after->addPixmap(_preview_image.ToQPixmap());
    _scene_adj_after->setSceneRect(_preview_image.ToQPixmap().rect());
    _ui->view_adj_after->setScene(_scene_adj_after);
    _ui->view_adj_after->fitInView(_scene_adj_after->itemsBoundingRect(), Qt::KeepAspectRatio);
}

/** SLOTS **/

void ColorAdjustDialog::on_bright_slider_valueChanged(int value)
{
    if (_ui->bright_val->text().toInt() != value)
    {
        _ui->bright_val->setText(QString::number(value));
        UpdatePreview();
    }
}

void ColorAdjustDialog::on_contr_slider_valueChanged(int value)
{
    if (_ui->contr_val->text().toInt() != value)
    {
        _ui->contr_val->setText(QString::number(value));
        UpdatePreview();
    }
}

void ColorAdjustDialog::on_bright_val_textChanged(const QString &arg1)
{
    int value = CLAMP(arg1.toInt(), -255, 255);

    if (_ui->bright_slider->value() != value)
    {
        _ui->bright_slider->setValue(value);
        UpdatePreview();
    }
}

void ColorAdjustDialog::on_contr_val_textChanged(const QString &arg1)
{
    int value = CLAMP(arg1.toInt(), 1, 255);

    if (_ui->contr_slider->value() != value)
    {
        _ui->contr_slider->setValue(value);
        UpdatePreview();
    }
}

/** EVENTS **/

void ColorAdjustDialog::showEvent(QShowEvent *event)
{
    _ui->view_adj_before->fitInView(_scene_adj_before->itemsBoundingRect(), Qt::KeepAspectRatio);
    _ui->view_adj_after->fitInView(_scene_adj_after->itemsBoundingRect(), Qt::KeepAspectRatio);
    QDialog::showEvent(event);
}
