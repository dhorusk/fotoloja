#ifndef FOTOLOJA_H
#define FOTOLOJA_H

#include <QImage>
#include <QPixmap>

#define FOREACH_PIXEL(image, x, y) \
for (y = 0; y < image.height(); ++y) \
    for (x = 0; x < image.width(); ++x)

#define CLAMP(x, min, max) \
x < min ? min : \
x > max ? max : \
x

#define GRAYSCALE_R 0.299
#define GRAYSCALE_G 0.587
#define GRAYSCALE_B 0.114

class FotoLojaImage
{
public:
    FotoLojaImage();
    FotoLojaImage(QString filename);
    FotoLojaImage(QImage img);
    ~FotoLojaImage() {}

    bool isGrayscale() const { return _is_grayscale; }
    QString GetFileName() const { return _filename; }
    uint GetWidth() const { return _image.width(); }
    uint GetHeight() const { return _image.height(); }
    QImage ToQImage() const { return _image; }
    QPixmap ToQPixmap() const { return QPixmap::fromImage(_image); }

    void SaveImage(QString filename = "");
    void ConvertToGrayscale();
    void ConvertToNegative();
    void FlipImage(bool vert, bool horiz);
    void RotateLeft();
    void RotateRight();
    void QuantizeGrayscale(int shades);
    void AdjustBrightness(int amount);
    void AdjustContrast(double amount);
    void Equalize();
    void Convolve(QVector< QVector<double> > kernel, bool boost = true);
    void ZoomIn();
    void ZoomOut(int window_width, int window_height);

private:
    QRgb PixelToLuminance(QRgb px);

    bool _is_grayscale;
    QString _filename;
    QImage _image;
};

#endif // FOTOLOJA_H
