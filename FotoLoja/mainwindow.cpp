#include <QFileDialog>
#include <QDropEvent>
#include <QMimeData>
#include <QInputDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "histogramdialog.h"
#include "coloradjustdialog.h"
#include "kernelvaluesdialog.h"
#include "zoomvaluesdialog.h"
#include "fotolojaimage.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
    _scene_before = new QGraphicsScene(this);
    _scene_after = new QGraphicsScene(this);

    _ui->setupUi(this);

    setAcceptDrops(true);
    _ui->view_img_before->setAcceptDrops(false);
    _ui->view_img_after->setAcceptDrops(false);

    _loaded_state = false;
    MenuBarSetEnabledActions();
}

MainWindow::~MainWindow()
{
    _scene_before->clear();
    _scene_after->clear();

    delete _scene_before;
    delete _scene_after;
    delete _ui;
}

/** HELPERS **/

// set enabled/disabled actions throughout the menubar
void MainWindow::MenuBarSetEnabledActions()
{
    _ui->actionReopen->setEnabled(_loaded_state);
    _ui->actionSave->setEnabled(_loaded_state);
    _ui->actionSave_As->setEnabled(_loaded_state);
    _ui->actionHistograms->setEnabled(_loaded_state);
    _ui->menuZoom->setEnabled(_loaded_state);
    _ui->actionAdjust_colors->setEnabled(_loaded_state);
    _ui->actionFlip_Vertical->setEnabled(_loaded_state);
    _ui->actionFlip_Horizontal->setEnabled(_loaded_state);
    _ui->actionRotate_Left->setEnabled(_loaded_state);
    _ui->actionRotate_Right->setEnabled(_loaded_state);
    _ui->actionGrayscale->setEnabled(_loaded_state);
    _ui->actionNegative->setEnabled(_loaded_state);
    _ui->actionEqualize->setEnabled(_loaded_state);
    _ui->actionQuantize->setEnabled(_loaded_state);
    _ui->menuConvolve->setEnabled(_loaded_state);
}

// update a view with a new image
void MainWindow::DisplayImage(QPixmap img_pm, QGraphicsScene *scene, QGraphicsView *view)
{
    scene->clear();
    scene->addPixmap(img_pm);
    scene->setSceneRect(img_pm.rect());
    view->setScene(scene);
}

// update 'original' view with a new image
void MainWindow::DisplayImageBefore(QPixmap img_pm)
{
    DisplayImage(img_pm, _scene_before, _ui->view_img_before);
}

// update 'current' view with a new image
void MainWindow::DisplayImageAfter(QPixmap img_pm)
{
    DisplayImage(img_pm, _scene_after, _ui->view_img_after);
}

// load a new image onto both views and update menus
void MainWindow::LoadImage(QString filename)
{
    if (filename == "")
        return;

    _orig_image = FotoLojaImage(filename);
    _cur_image = FotoLojaImage(filename);

    DisplayImageBefore(_orig_image.ToQPixmap());
    DisplayImageAfter(_cur_image.ToQPixmap());

    _loaded_state = true;
    MenuBarSetEnabledActions();
}

/** SLOTS **/

void MainWindow::on_actionOpen_triggered()
{
    // dialog to open a *.jpg or *.jpeg file
    QString filename = QFileDialog::getOpenFileName(this,
        tr("Open image"), "", tr("JPEG files (*.jpg *.jpeg)"));

    // user may have closed the dialog
    if (!filename.isEmpty())
        LoadImage(filename);
}

void MainWindow::on_actionReopen_triggered()
{
    _cur_image = _orig_image;
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionSave_triggered()
{
    _cur_image.SaveImage();
}

void MainWindow::on_actionSave_As_triggered()
{
    // dialog to open a *.jpg or *.jpeg file
    QString new_filename = QFileDialog::getSaveFileName(this,
        tr("Save image"), _cur_image.GetFileName(),
        tr("JPEG file (*.jpg *.jpeg)"));

    // user may have closed the dialog
    if (!new_filename.isEmpty())
        _cur_image.SaveImage(new_filename);
}

void MainWindow::on_actionExit_triggered()
{
    qApp->exit();
}

void MainWindow::on_actionUndo_triggered()
{

}

void MainWindow::on_actionRedo_triggered()
{

}

void MainWindow::on_actionHistograms_triggered()
{
    HistogramDialog hist_dlg(_orig_image, _cur_image);
    hist_dlg.exec();
}

void MainWindow::on_actionZoom_In_triggered()
{
    _cur_image.ZoomIn();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionZoom_Out_triggered()
{
    ZoomValuesDialog zoom_val_dlg(this);
    zoom_val_dlg.exec();
    double horiz = zoom_val_dlg.GetHorizontal();
    double vert = zoom_val_dlg.GetVertical();
    _cur_image.ZoomOut(horiz, vert);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionAdjust_colors_triggered()
{
    ColorAdjustDialog color_adj_dlg(_cur_image, this);
    if (color_adj_dlg.exec())
    {
        _cur_image = color_adj_dlg.GetPreview();
        DisplayImageAfter(_cur_image.ToQPixmap());
    }
}

void MainWindow::on_actionFlip_Vertical_triggered()
{
    _cur_image.FlipImage(true, false);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionFlip_Horizontal_triggered()
{
    _cur_image.FlipImage(false, true);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionRotate_Right_triggered()
{
    _cur_image.RotateRight();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionRotate_Left_triggered()
{
    _cur_image.RotateLeft();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionGrayscale_triggered()
{
    _cur_image.ConvertToGrayscale();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionNegative_triggered()
{
    _cur_image.ConvertToNegative();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionEqualize_triggered()
{
    _cur_image.Equalize();
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionQuantize_triggered()
{
    // dialog to open a *.jpg or *.jpeg file
    int shades = QInputDialog::getInt(this, tr("Quantize a grayscale image"),
                                      tr("Number of shades:"), 0, 1, 255, 1);

    // user may have closed the dialog
    if (shades != 0)
    {
        _cur_image.QuantizeGrayscale(shades);
        DisplayImageAfter(_cur_image.ToQPixmap());
    }
}

void MainWindow::on_actionGaussian_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{0.0625, 0.0125, 0.0625},
        QVector<double>{0.0125, 0.25,   0.0125},
        QVector<double>{0.0625, 0.0125, 0.0625}
    };
    _cur_image.Convolve(kernel, false);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionLaplacian_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{ 0.0, -1.0,  0.0},
        QVector<double>{-1.0,  4.0, -1.0},
        QVector<double>{ 0.0, -1.0,  0.0}
    };
    _cur_image.Convolve(kernel, false);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionHigh_pass_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{-1.0, -1.0, -1.0},
        QVector<double>{-1.0,  8.0, -1.0},
        QVector<double>{-1.0, -1.0, -1.0}
    };
    _cur_image.Convolve(kernel, false);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionPrewitt_Horizontal_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{-1.0, 0.0, 1.0},
        QVector<double>{-1.0, 0.0, 1.0},
        QVector<double>{-1.0, 0.0, 1.0}
    };
    _cur_image.Convolve(kernel);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionPrewitt_Vertical_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{-1.0, -1.0, -1.0},
        QVector<double>{ 0.0,  0.0,  0.0},
        QVector<double>{ 1.0,  1.0,  1.0}
    };
    _cur_image.Convolve(kernel);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionSobel_Horizontal_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{-1.0, 0.0, 1.0},
        QVector<double>{-2.0, 0.0, 2.0},
        QVector<double>{-1.0, 0.0, 1.0}
    };
    _cur_image.Convolve(kernel);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionSobel_Vertical_triggered()
{
    QVector< QVector<double> > kernel {
        QVector<double>{-1.0, -2.0, -1.0},
        QVector<double>{ 0.0,  0.0,  0.0},
        QVector<double>{ 1.0,  2.0,  1.0}
    };
    _cur_image.Convolve(kernel);
    DisplayImageAfter(_cur_image.ToQPixmap());
}

void MainWindow::on_actionCustom_Kernel_triggered()
{
    KernelValuesDialog kernel_vals_dlg(this);
    if (kernel_vals_dlg.exec())
    {
        QVector< QVector<double> > kernel = kernel_vals_dlg.GetKernel();
        _cur_image.Convolve(kernel);
        DisplayImageAfter(_cur_image.ToQPixmap());
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, "About FotoLoja :^)",
                       "<p>FotoLoja :^) v2.02<br />"
                       "Written by Daniel Kelling<br />"
                       "Licensed under GNU LGPL v3.0</p>");
}

/** EVENTS **/

// open files dropped from explorer
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    const QMimeData *md = event->mimeData();

    // take the first file only
    if (md->hasUrls())
    {
        QString filename = md->urls().first().toLocalFile();
        LoadImage(filename);
        event->acceptProposedAction();
    }
}
