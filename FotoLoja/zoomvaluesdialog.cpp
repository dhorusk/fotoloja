#include "zoomvaluesdialog.h"
#include "ui_zoomvaluesdialog.h"

ZoomValuesDialog::ZoomValuesDialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::ZoomValuesDialog)
{
    _ui->setupUi(this);
}

ZoomValuesDialog::~ZoomValuesDialog()
{
    delete _ui;
}

void ZoomValuesDialog::on_buttonBox_accepted()
{
    _horiz = _ui->horiz->text().toDouble();
    _vert = _ui->vert->text().toDouble();
    this->close();
}
