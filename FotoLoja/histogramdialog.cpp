#include "histogramdialog.h"
#include "ui_histogramdialog.h"
#include "histogram.h"

HistogramDialog::HistogramDialog(FotoLojaImage fl_before,
                                 FotoLojaImage fl_after, QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::HistogramDialog)
{
    _ui->setupUi(this);

    Histogram hist_before(fl_before);
    Histogram hist_after(fl_after);

    QPixmap hist_before_pm = hist_before.ToPixmap();
    QPixmap hist_after_pm = hist_after.ToPixmap();

    _scene_hist_before = new QGraphicsScene(this);
    _scene_hist_after = new QGraphicsScene(this);

    _scene_hist_before->clear();
    _scene_hist_before->addPixmap(hist_before_pm);
    _scene_hist_before->setSceneRect(hist_before_pm.rect());
    _ui->view_hist_before->setScene(_scene_hist_before);

    _scene_hist_after->clear();
    _scene_hist_after->addPixmap(hist_after_pm);
    _scene_hist_after->setSceneRect(hist_after_pm.rect());
    _ui->view_hist_after->setScene(_scene_hist_after);
}

HistogramDialog::~HistogramDialog()
{
    delete _ui;
    delete _scene_hist_before;
    delete _scene_hist_after;
}

void HistogramDialog::on_buttonBox_clicked(QAbstractButton *)
{
    this->close();
}
