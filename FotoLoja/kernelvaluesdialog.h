#ifndef KERNELVALUESDIALOG_H
#define KERNELVALUESDIALOG_H

#include <QDialog>

namespace Ui {
class KernelValuesDialog;
}

class KernelValuesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KernelValuesDialog(QWidget *parent = 0);
    ~KernelValuesDialog();
    QVector< QVector<double> > GetKernel() const { return _kernel; }

private slots:
    void on_buttonBox_accepted();

private:
    Ui::KernelValuesDialog *_ui;
    QVector< QVector<double> > _kernel {
        QVector<double>{0, 0, 0},
        QVector<double>{0, 0, 0},
        QVector<double>{0, 0, 0}
    };
};

#endif // KERNELVALUESDIALOG_H
