#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsView>
#include <QMainWindow>
#include "fotolojaimage.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);

private slots:
    void on_actionOpen_triggered();
    void on_actionReopen_triggered();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_actionExit_triggered();
    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionHistograms_triggered();
    void on_actionZoom_In_triggered();
    void on_actionZoom_Out_triggered();
    void on_actionAdjust_colors_triggered();
    void on_actionFlip_Vertical_triggered();
    void on_actionFlip_Horizontal_triggered();
    void on_actionRotate_Right_triggered();
    void on_actionRotate_Left_triggered();
    void on_actionGrayscale_triggered();
    void on_actionNegative_triggered();
    void on_actionEqualize_triggered();
    void on_actionQuantize_triggered();
    void on_actionGaussian_triggered();
    void on_actionLaplacian_triggered();
    void on_actionHigh_pass_triggered();
    void on_actionPrewitt_Horizontal_triggered();
    void on_actionPrewitt_Vertical_triggered();
    void on_actionSobel_Horizontal_triggered();
    void on_actionSobel_Vertical_triggered();
    void on_actionCustom_Kernel_triggered();
    void on_actionAbout_triggered();

private:
    void MenuBarSetEnabledActions();
    void DisplayImage(QPixmap img_pm, QGraphicsScene *scene, QGraphicsView *view);
    void DisplayImageBefore(QPixmap img_pm);
    void DisplayImageAfter(QPixmap img_pm);
    void LoadImage(QString filename);

    Ui::MainWindow *_ui;
    QGraphicsScene *_scene_before;
    QGraphicsScene *_scene_after;
    FotoLojaImage _orig_image;
    FotoLojaImage _cur_image;
    bool _loaded_state;
};

#endif // MAINWINDOW_H
