#ifndef COLORADJUSTDIALOG_H
#define COLORADJUSTDIALOG_H

#include <QAbstractButton>
#include <QGraphicsScene>
#include <QDialog>
#include "fotolojaimage.h"

namespace Ui {
class ColorAdjustDialog;
}

class ColorAdjustDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ColorAdjustDialog(FotoLojaImage image, QWidget *parent = 0);
    ~ColorAdjustDialog();
    FotoLojaImage GetPreview() const { return _preview_image; }

private slots:
    void on_bright_slider_valueChanged(int value);
    void on_contr_slider_valueChanged(int value);
    void on_bright_val_textChanged(const QString &arg1);
    void on_contr_val_textChanged(const QString &arg1);

protected:
    void showEvent(QShowEvent *event);

private:
    void UpdatePreview();

    Ui::ColorAdjustDialog *_ui;
    QGraphicsScene *_scene_adj_before;
    QGraphicsScene *_scene_adj_after;
    FotoLojaImage _cur_image;
    FotoLojaImage _preview_image;
};

#endif // COLORADJUSTDIALOG_H
