#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QImage>
#include "fotolojaimage.h"

#define HISTOGRAM_SIZE_X 256

class Histogram
{
public:
    Histogram();
    Histogram(FotoLojaImage fl_img);
    Histogram(FotoLojaImage *fl_img);
    Histogram(QImage img, bool grayscale);
    ~Histogram();
    QVector<int> GetRawData() const { return _color_data; }
    QRgb Equalize_Shade(QRgb color);
    QPixmap ToPixmap();

private:
    void CreateEqualizationLookup();

    QVector<int> _color_data;
    QVector<int> _equalization_lookup;
    bool _equalized;
    int _n_pixels;
};

#endif // HISTOGRAM_H
