#include <stdexcept>
#include "fotolojaimage.h"
#include "histogram.h"

/** CTORS **/

FotoLojaImage::FotoLojaImage() :
    FotoLojaImage(QImage()) {}

FotoLojaImage::FotoLojaImage(QString filename) :
    FotoLojaImage(QImage(filename))
{
    _filename = filename;
}

FotoLojaImage::FotoLojaImage(QImage img)
{
    _image = img;
    _filename = "";
    _is_grayscale = _image.isGrayscale();

    if (_is_grayscale)
        _image = _image.convertToFormat(QImage::Format_Grayscale8);
}

/** PUBLIC METHODS **/

// save image
void FotoLojaImage::SaveImage(QString filename /* = "" */)
{
    if (filename.isEmpty())
        filename = _filename;

    if (_image.save(filename))
        _filename = filename;
}

// flip an image vertically, horizontally or both
void FotoLojaImage::FlipImage(bool vert, bool horiz)
{
    // no change
    if (!vert && !horiz)
        return;

    QImage image_cpy = _image.copy();
    int x_max = _image.width() - 1;
    int y_max = _image.height() - 1;
    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        int src_x = horiz ? x_max-x : x;
        int src_y = vert ? y_max-y : y;
        QRgb new_px = image_cpy.pixel(src_x, src_y);
        _image.setPixel(x, y, new_px);
    }
}

// rotate image 90º counter-clockwise
void FotoLojaImage::RotateLeft()
{
    QImage new_image(_image.height(), _image.width(), _image.format());
    int x_max = _image.width() - 1;

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        int new_x = y;
        int new_y = x_max-x;
        new_image.setPixel(new_x, new_y, px);
    }

    _image = new_image;
}

// rotate image 90º clockwise
void FotoLojaImage::RotateRight()
{
    QImage new_image(_image.height(), _image.width(), _image.format());
    int y_max = _image.height() - 1;

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        int new_x = y_max-y;
        int new_y = x;
        new_image.setPixel(new_x, new_y, px);
    }

    _image = new_image;
}

// convert image to grayscale
void FotoLojaImage::ConvertToGrayscale()
{
    // no change
    if (_is_grayscale)
        return;

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        QRgb new_px = PixelToLuminance(px);
        _image.setPixel(x, y, new_px);
    }

    _is_grayscale = true;

    // only needed cause it was 24BPP, and it's 8BPP now
    _image = _image.convertToFormat(QImage::Format_Grayscale8);
}

// convert image to negative
void FotoLojaImage::ConvertToNegative()
{
    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        QRgb r = CLAMP(255 - qRed(px), 0, 255);
        QRgb g = CLAMP(255 - qGreen(px), 0, 255);
        QRgb b = CLAMP(255 - qBlue(px), 0, 255);
        QRgb new_px = qRgb(r, g, b);
        _image.setPixel(x, y, new_px);
    }
}

// quantize grayscale image (reduce number of shades used)
// if image is not grayscale, it is converted before quantization
void FotoLojaImage::QuantizeGrayscale(int shades)
{
    if (shades < 1 || shades > 255)
        throw std::invalid_argument("shades");

    if (!_is_grayscale)
        ConvertToGrayscale();

    double band_size = 256 / shades;
    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = qRed(_image.pixel(x, y)); // R=G=B, so any will work
        int band_no = px / band_size;
        QRgb new_px_val = band_no*band_size;
        QRgb new_px = qRgb(new_px_val, new_px_val, new_px_val);
        _image.setPixel(x, y, new_px);
    }
}

// adjust brightness (add or subtract from each px)
void FotoLojaImage::AdjustBrightness(int amount)
{
    if (amount < -255 || amount > 255)
        throw std::invalid_argument("amount");

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        QRgb r = CLAMP(qRed(px) + amount, 0, 255);
        QRgb g = CLAMP(qGreen(px) + amount, 0, 255);
        QRgb b = CLAMP(qBlue(px) + amount, 0, 255);
        QRgb new_px = qRgb(r, g, b);
        _image.setPixel(x, y, new_px);
    }
}

// adjust contrast (multiply or divide each px)
void FotoLojaImage::AdjustContrast(double amount)
{
    if (amount <= 0 || amount > 255)
        throw std::invalid_argument("amount");

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb px = _image.pixel(x, y);
        QRgb r = CLAMP(qRed(px) * amount, 0, 255);
        QRgb g = CLAMP(qGreen(px) * amount, 0, 255);
        QRgb b = CLAMP(qBlue(px) * amount, 0, 255);
        QRgb new_px = qRgb(r, g, b);
        _image.setPixel(x, y, new_px);
    }
}

// equalize image based on grayscale histogram
void FotoLojaImage::Equalize()
{
    Histogram hist = Histogram(this);

    int x, y;
    FOREACH_PIXEL(_image, x, y)
    {
        QRgb r = qRed(_image.pixel(x, y));
        QRgb new_r = hist.Equalize_Shade(r);
        QRgb g = qGreen(_image.pixel(x, y));
        QRgb new_g = hist.Equalize_Shade(g);
        QRgb b = qBlue(_image.pixel(x, y));
        QRgb new_b = hist.Equalize_Shade(b);
        QRgb new_px = qRgb(new_r, new_g, new_b);
        _image.setPixel(x, y, new_px);
    }
}

// convolve image with the given 3x3 kernel
void FotoLojaImage::Convolve(QVector< QVector<double> > kernel, bool boost /* = true */)
{
    if (_image.width() < 5 || _image.height() < 5)
        return;

    if (!_is_grayscale)
        ConvertToGrayscale();

    QImage image_cpy = _image.copy();
    int x_max = _image.width() - 1;
    int y_max = _image.height() - 1;

    // don't process image borders
    for (int y = 1; y < y_max; ++y)
    {
        for (int x = 1; x < x_max; ++x)
        {
            // multiply each pixel with the correponding 180º-spun kernel multiplier
            double conv = kernel[0][0] * qRed(image_cpy.pixel(x+1, y+1)) + // R=G=B anyway
                          kernel[0][1] * qRed(image_cpy.pixel(x  , y+1)) +
                          kernel[0][2] * qRed(image_cpy.pixel(x-1, y+1)) +
                          kernel[1][0] * qRed(image_cpy.pixel(x+1, y  )) +
                          kernel[1][1] * qRed(image_cpy.pixel(x  , y  )) +
                          kernel[1][2] * qRed(image_cpy.pixel(x-1, y  )) +
                          kernel[2][0] * qRed(image_cpy.pixel(x+1, y-1)) +
                          kernel[2][1] * qRed(image_cpy.pixel(x  , y-1)) +
                          kernel[2][2] * qRed(image_cpy.pixel(x-1, y-1));
            if (boost)
                conv += 127;
            QRgb new_color = CLAMP(conv, 0, 255);
            QRgb new_px = qRgb(new_color, new_color, new_color);
            _image.setPixel(x, y, new_px);
        }
    }
}

// zoom image out to 200%
void FotoLojaImage::ZoomIn()
{
    int new_width = _image.width()*2 - 1;
    int new_height = _image.height()*2 - 1;
    QImage new_image = QImage(new_width, new_height, _image.format());
    int x, y;

    // copy original pixels
    FOREACH_PIXEL(new_image, x, y)
    {
        if (x % 2 == 0 && y % 2 == 0) // both even
        {
            QRgb px = _image.pixel(x/2, y/2);
            new_image.setPixel(x, y, px);
        }
    }

    // simple interpolations
    FOREACH_PIXEL(new_image, x, y)
    {
        if ((x+y) % 2 == 0) // both even or both odd
            continue;

        if (x % 2 == 0 && y % 2 == 1) // x even, y odd
        {
            double temp_r = (qRed(new_image.pixel(x, y-1)) +
                             qRed(new_image.pixel(x, y+1))) / 2.0;
            double temp_g = (qGreen(new_image.pixel(x, y-1)) +
                             qGreen(new_image.pixel(x, y+1))) / 2.0;
            double temp_b = (qBlue(new_image.pixel(x, y-1)) +
                             qBlue(new_image.pixel(x, y+1))) / 2.0;
            int r = CLAMP(temp_r, 0, 255);
            int g = CLAMP(temp_g, 0, 255);
            int b = CLAMP(temp_b, 0, 255);
            QRgb px = qRgb(r, g, b);
            new_image.setPixel(x, y, px);
        }
        else if (x % 2 == 1 && y % 2 == 0) // x odd, y even
        {
            double temp_r = (qRed(new_image.pixel(x-1, y)) +
                             qRed(new_image.pixel(x+1, y))) / 2.0;
            double temp_g = (qGreen(new_image.pixel(x-1, y)) +
                             qGreen(new_image.pixel(x+1, y))) / 2.0;
            double temp_b = (qBlue(new_image.pixel(x-1, y)) +
                             qBlue(new_image.pixel(x+1, y))) / 2.0;
            int r = CLAMP(temp_r, 0, 255);
            int g = CLAMP(temp_g, 0, 255);
            int b = CLAMP(temp_b, 0, 255);
            QRgb px = qRgb(r, g, b);
            new_image.setPixel(x, y, px);
        }
    }

    // complex interpolations
    FOREACH_PIXEL(new_image, x, y)
    {
        if (x % 2 == 0 || y % 2 == 0) // at least one even
            continue;

        double temp_r = (qRed(new_image.pixel(x-1, y)) +
                         qRed(new_image.pixel(x+1, y)) +
                         qRed(new_image.pixel(x, y-1)) +
                         qRed(new_image.pixel(x, y+1))) / 4.0;
        double temp_g = (qGreen(new_image.pixel(x-1, y)) +
                         qGreen(new_image.pixel(x+1, y)) +
                         qGreen(new_image.pixel(x, y-1)) +
                         qGreen(new_image.pixel(x, y+1))) / 4.0;
        double temp_b = (qBlue(new_image.pixel(x-1, y)) +
                         qBlue(new_image.pixel(x+1, y)) +
                         qBlue(new_image.pixel(x, y-1)) +
                         qBlue(new_image.pixel(x, y+1))) / 4.0;
        int r = CLAMP(temp_r, 0, 255);
        int g = CLAMP(temp_g, 0, 255);
        int b = CLAMP(temp_b, 0, 255);
        QRgb px = qRgb(r, g, b);
        new_image.setPixel(x, y, px);
    }

    _image = new_image;
}

// zoom image out with given reduction factors
// it helps picturing a window with these sizes
// overlaid on top of the image to generate a single pixel
void FotoLojaImage::ZoomOut(int window_width, int window_height)
{
    int new_width = ceil(1.0*_image.width()/window_width);
    int new_height = ceil(1.0*_image.height()/window_height);
    int x_max = _image.width() - 1;
    int y_max = _image.height() - 1;
    QImage new_image(new_width, new_height, _image.format());

    int x, y;
    FOREACH_PIXEL(new_image, x, y)
    {
        int n_px = 0;
        double temp_r = 0;
        double temp_g = 0;
        double temp_b = 0;

        for (int j = y*window_height; j < y*window_height + window_height; ++j)
        {
            for (int i = x*window_width; i < x*window_width + window_width; ++i)
            {
                if (i > x_max || j > y_max)
                    continue;

                QRgb temp_px = _image.pixel(i, j);
                n_px += 1;
                temp_r += qRed(temp_px);
                temp_g += qGreen(temp_px);
                temp_b += qBlue(temp_px);
            }
        }

        int r = CLAMP((temp_r/n_px), 0, 255);
        int g = CLAMP((temp_g/n_px), 0, 255);
        int b = CLAMP((temp_b/n_px), 0, 255);
        QRgb px = qRgb(r, g, b);
        new_image.setPixel(x, y, px);
    }

    _image = new_image;
}

/** PRIVATE METHODS **/

// convert a pixel value to luminance
QRgb FotoLojaImage::PixelToLuminance(QRgb px)
{
    QRgb luminance = qRed(px)*GRAYSCALE_R +
                     qGreen(px)*GRAYSCALE_G +
                     qBlue(px)*GRAYSCALE_B;

    return qRgb(luminance, luminance, luminance);
}
