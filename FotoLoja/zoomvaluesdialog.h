#ifndef ZOOMVALUESDIALOG_H
#define ZOOMVALUESDIALOG_H

#include <QDialog>

namespace Ui {
class ZoomValuesDialog;
}

class ZoomValuesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ZoomValuesDialog(QWidget *parent = 0);
    ~ZoomValuesDialog();
    double GetHorizontal() const { return _horiz; }
    double GetVertical() const { return _vert; }

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ZoomValuesDialog *_ui;
    double _horiz;
    double _vert;
};

#endif // ZOOMVALUESDIALOG_H
