#include <QPainter>
#include <algorithm>
#include <stdexcept>
#include "histogram.h"

// ctors
Histogram::Histogram() :
    Histogram(QImage(), true){}

Histogram::Histogram(FotoLojaImage fl_img) :
    Histogram(&fl_img){}

Histogram::Histogram(FotoLojaImage *fl_img) :
    Histogram(fl_img->ToQImage(), fl_img->isGrayscale()){}

Histogram::Histogram(QImage img, bool grayscale)
{
    _color_data = QVector<int>(HISTOGRAM_SIZE_X, 0);
    _equalization_lookup = QVector<int>(HISTOGRAM_SIZE_X, 0);

    if (!grayscale)
    {
        FotoLojaImage fl = FotoLojaImage(img);
        fl.ConvertToGrayscale();
        img = fl.ToQImage();
    }

    int x, y;
    FOREACH_PIXEL(img, x, y)
    {
        QRgb px = qRed(img.pixel(x, y)); // R=G=B anyway
        _color_data[px] += 1;
    }

    _equalized = false;
    _n_pixels = img.width()*img.height();
}

// dtor
Histogram::~Histogram()
{
    _color_data.clear();
    _color_data.resize(0);

    _equalization_lookup.clear();
    _equalization_lookup.resize(0);
}

// equalize a monochrome shade based on this histogram
QRgb Histogram::Equalize_Shade(QRgb color)
{
    if (color > 255)
        throw std::invalid_argument("color");

    if (!_equalized)
        CreateEqualizationLookup();

    return _equalization_lookup[color];
}

// create a 256*256 pixmap representing this histogram
QPixmap Histogram::ToPixmap()
{
    QPixmap pm(256, 256);
    QPainter p(&pm);

    pm.fill(Qt::gray);
    int max = *std::max_element(_color_data.begin(), _color_data.end());
    for(int i = 0; i < 256; ++i)
    {
        int scaled_val = _color_data[i] * 255 / max;
        p.drawLine(i, 256, i, 256-scaled_val);
    }

    return pm;
}

// create the lookup vector for histogram equalization
void Histogram::CreateEqualizationLookup()
{
    if (_n_pixels == 0)
    {
        _equalization_lookup = _color_data;
        return;
    }

    double scale_factor = 255.0 / _n_pixels;
    _equalization_lookup.clear();

    // normalized cumulative histogram
    _equalization_lookup[0] = scale_factor * _color_data[0];
    for (int i = 1; i < HISTOGRAM_SIZE_X; ++i)
    {
        int cur_val = scale_factor * _color_data[i];
        _equalization_lookup[i] = _equalization_lookup[i-1] + cur_val;
    }

    _equalized = true;
}
