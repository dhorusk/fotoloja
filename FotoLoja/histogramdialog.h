#ifndef HISTOGRAMDIALOG_H
#define HISTOGRAMDIALOG_H

#include <QAbstractButton>
#include <QGraphicsScene>
#include <QDialog>
#include "fotolojaimage.h"

namespace Ui {
class HistogramDialog;
}

class HistogramDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HistogramDialog(FotoLojaImage fl_before, FotoLojaImage fl_after, QWidget *parent = 0);
    ~HistogramDialog();

private slots:
    void on_buttonBox_clicked(QAbstractButton *);

private:
    Ui::HistogramDialog *_ui;
    QGraphicsScene *_scene_hist_before;
    QGraphicsScene *_scene_hist_after;
};

#endif // HISTOGRAMDIALOG_H
