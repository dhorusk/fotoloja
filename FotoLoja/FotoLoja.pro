QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FotoLoja
TEMPLATE = app

SOURCES += main.cpp \
           mainwindow.cpp \
           fotolojaimage.cpp \
           histogram.cpp \
           histogramdialog.cpp \
           coloradjustdialog.cpp \
           kernelvaluesdialog.cpp \
           zoomvaluesdialog.cpp

HEADERS += mainwindow.h \
           fotolojaimage.h \
           histogram.h \
           histogramdialog.h \
           coloradjustdialog.h \
           kernelvaluesdialog.h \
           zoomvaluesdialog.h

FORMS += mainwindow.ui \
         histogramdialog.ui \
         coloradjustdialog.ui \
         kernelvaluesdialog.ui \
         zoomvaluesdialog.ui

win32:RC_ICONS += res/icon32.ico
win32:RC_ICONS += res/icon64.ico
win32:RC_ICONS += res/icon128.ico
win32:RC_ICONS += res/icon256.ico
